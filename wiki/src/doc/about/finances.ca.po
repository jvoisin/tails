# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-04-12 16:10+0200\n"
"PO-Revision-Date: 2018-07-02 10:45+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Finances\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"partners\"></a>\n"
msgstr "<a id=\"partners\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Partners"
msgstr ""

#. type: Plain text
msgid ""
"Grants, awards, corporate donations, and substantial donations from "
"individuals are listed in a more visual way on our [[partners page|"
"partners]]."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "<a id=\"2018\"></a>\n"
msgstr "<a id=\"2016\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Income statement for 2018"
msgstr ""

#, fuzzy
#~ msgid "<a id=\"2017\"></a>\n"
#~ msgstr "<a id=\"2016\"></a>\n"

#~ msgid "<a id=\"2016\"></a>\n"
#~ msgstr "<a id=\"2016\"></a>\n"
