# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-08-29 08:06+0000\n"
"PO-Revision-Date: 2018-10-26 13:16+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sr_Latn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Logging in to captive portals\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/captive_portal.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tor cannot be started when your Internet connection is initially blocked\n"
"by a captive portal. So, Tails includes an\n"
"<span class=\"application\">Unsafe Browser</span> to log in to captive\n"
"portals before starting Tor.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To start the <span class=\"application\">Unsafe Browser</span>, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">Unsafe Web Browser</span></span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The <span class=\"application\">Unsafe Browser</span> has a red theme to\n"
"differentiate it from [[<span class=\"application\">Tor Browser</span>|Tor_Browser]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p><strong>The <span class=\"application\">Unsafe Browser</span> is not\n"
"anonymous</strong>. Use it only to log in to captive portals or to\n"
"[[browse web pages on the local network|advanced_topics/lan#browser]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""

#. type: Plain text
msgid "Security recommendations:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Do not run this browser at the same time as the anonymous [[<span class="
"\"application\">Tor Browser</span>|Tor_Browser]]. This makes it easy to not "
"mistake one browser for the other, which could have catastrophic "
"consequences."
msgstr ""

msgid ""
"Many publicly accessible Internet connections (usually available through a "
"wireless network connection) require its users to register and login in "
"order to get access to the Internet. This include both free and paid for "
"services that may be found at Internet cafés, libraries, airports, hotels, "
"universities etc. Normally in these situations, a so called *captive portal* "
"intercepts any website request made and redirects the web browser to a login "
"page. None of that works when Tor is used, so a browser with unrestricted "
"network access is necessary."
msgstr ""
"Viele öffentlich zugängliche Internetverbindungen (auf die üblicherweise "
"über eine drahtlose Netzwerkverbindung zugegriffen werden kann) verlangen "
"von ihren Nutzenden, sich zu registrieren und anzumelden, um Internetzugriff "
"zu erhalten. Dies umfasst sowohl kostenlose als auch kostenpflichtige "
"Dienste, die z. B. in Internetcafé's, Bibliotheken, Flughäfen, Hotels, "
"Universtitäten usw. aufgefunden werden können. Normalerweise werden in "
"solchen Situationen *Captive Portals* jegliche Anfragen zu Websites abfangen "
"und den Webbrowser zu einer Loginseite weiterleiten. Nichts davon "
"funktioniert, wenn Tor benutzt wird, somit ist ein Browser mit "
"uneingeschränktem Internetzugriff nötig."

msgid ""
"When using [[doc/first_steps/welcome_screen/windows_camouflage]] the red "
"theme is disabled in order to raise less suspicion. It is still possible to "
"quietly identify the <span class=\"application\">Unsafe Browser</span> since "
"it has English Wikipedia as its default (and only) search engine in the "
"navigation toolbar. The lack of the onion icon is another such visual "
"reminder."
msgstr ""
"Wenn [[Windows Camouflage|doc/first_steps/welcome_screen/"
"windows_camouflage]] benutzt wird, ist das rote Farbschema deaktiviert, um "
"weniger Aufmerksamkeit zu erregen. Es ist dennoch möglich, den <span class="
"\"application\">Unsicheren Browser</span> in Ruhe zu erkennen, da er die "
"englische Wikipedia als voreingestellte (und einzige) Suchmaschine in der "
"Adresszeile besitzt. Das Fehlen des Zwiebellogos ist ein weitere optische "
"Erinnerung."
