# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-04-23 16:49+0000\n"
"PO-Revision-Date: 2018-07-02 10:44+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Welcome Screen\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"The Welcome Screen appears after the Boot Loader, but before the GNOME "
"Desktop."
msgstr ""

#. type: Plain text
msgid ""
"You can use the Welcome Screen to specify startup options that alter some of "
"the basic functioning of Tails."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img welcome-screen.png link=no alt=\"Welcome to Tails!\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To start Tails without options, click on the\n"
"<span class=\"button\">Start Tails</span> button.\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "<a id=\"accessibility\"></a>\n"
msgstr "<a id=\"persistence\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Assistive technologies"
msgstr ""

#. type: Plain text
msgid ""
"You can activate assistive technologies, like a screen reader or large text, "
"from the universal access menu (the"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img lib/preferences-desktop-accessibility.png alt=\"Universal Access\" class=\"symbolic\" link=\"no\"]]\n"
msgstr ""

#. type: Plain text
msgid "icon which looks like a person) in the top bar."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"locale\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Language & region"
msgstr ""

#. type: Plain text
msgid ""
"You can configure Tails depending on your language and location from the "
"Welcome Screen."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img locale.png link=\"no\" alt=\"Language & Region section of the Welcome Screen\"]]\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Language</span> option allows you to change the "
"main language of the interface."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  Text that is not translated yet will appear in English. You can [[help\n"
"  to translate more text|contribute/how/translate]].\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Keyboard Layout</span> option allows you to "
"change the layout of the keyboard. For example to switch to an *AZERTY* "
"keyboard which is common in France."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  You will still be able to switch between different keyboard layouts from the\n"
"  desktop after starting Tails.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img introduction_to_gnome_and_the_tails_desktop/keyboard.png link=\"no\" alt=\"Menu in the top-right corner of the desktop to switch between different keyboard layouts\"]]\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"The <span class=\"guilabel\">Formats</span> option allows you to change the "
"date and time format, first day of the week, measurement units, and default "
"paper size according to the standards in use in a country."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  For example, the USA and the United Kingdom, two English-speaking countries,\n"
"  have different standards:\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  <table>\n"
"  <tr><th></th><th>USA</th><th>United Kingdom</th></tr>\n"
"  <tr><td>Date & time</td><td>3/17/2017 3:56 PM</td><td>17/03/2017 15:56</td></tr>\n"
"  <tr><td>First day of the week</td><td>Sunday</td><td>Monday</td></tr>\n"
"  <tr><td>Unit system</td><td>Imperial</td><td>Metric</td></tr>\n"
"  <tr><td>Paper size</td><td>Letter</td><td>A4</td></tr>\n"
"  </table>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  With this option you can also display the calendar in a different language\n"
"  than the main language. For example, to display a US calendar, with weeks\n"
"  starting on Sunday, when the main language is Russian.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img US_calendar_in_Russian.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"persistence\"></a>\n"
msgstr "<a id=\"persistence\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Persistent Storage"
msgstr ""

#. type: Plain text
msgid ""
"If a [[Persistent Storage|first_steps/persistence]] is detected on the USB "
"stick, an additional section appears in the Welcome Screen below the "
"**Language & Region** section:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img persistence.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"To unlock the Persistent Storage, enter your passphrase and click **Unlock**."
msgstr ""

#. type: Plain text
msgid ""
"To create a Persistent Storage, see our the instructions to [[create a "
"Persistent Storage|install/win/usb#create-persistence]] in our installation "
"instructions and our [[documentation on the Persistent Storage|persistence]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"additional\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Additional settings"
msgstr ""

#. type: Plain text
msgid ""
"Tails is configured with care to be as safe as possible by default. But, "
"depending on your situation, you can change one of the following settings "
"from the Welcome Screen."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img additional.png link=\"no\" alt=\"Additional settings of the Welcome Screen\"]]\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Set an <span class=\"guilabel\">Administration Password</span> to be able to "
"perform administrative tasks like installing additional software or "
"accessing the internal hard disks of the computer."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[See our documentation about the administration password.|administration_password]]\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Disable <span class=\"guilabel\">MAC Address Spoofing</span> to prevent "
"connectivity problems with your network interfaces."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[See our documentation about MAC address spoofing.|mac_spoofing]]\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Change the <span class=\"guilabel\">Network Configuration</span> to either:"
msgstr ""

#. type: Bullet: '  - '
msgid "Connect directly to the Tor network (default)."
msgstr ""

#. type: Bullet: '  - '
msgid "Configure a Tor bridge or local proxy:"
msgstr ""

#. type: Bullet: '    - '
msgid ""
"If you want to use Tor bridges because your Internet connection is censored "
"or you want to hide the fact that you are using Tor."
msgstr ""

#. type: Bullet: '    - '
msgid "If you need to use a local proxy to access the Internet."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    After starting Tails and connecting to a network, an assistant will\n"
"    guide you through the configuration of Tor.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    [[See our documentation about Tor bridges.|bridge_mode]]\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Disable all networking if you want to work completely offline with "
"additional security."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Keyboard shortcuts"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table>\n"
"<tr><td><span class=\"keycap\">Alt+L</span></td><td><span class=\"guilabel\">Language</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+K</span></td><td><span class=\"guilabel\">Keyboard Layout</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+F</span></td><td><span class=\"guilabel\">Formats</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+P</span></td><td><span class=\"guilabel\">Persistent Storage</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+A</span></td><td><span class=\"guilabel\">Additional Settings</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+A</span></td><td><span class=\"guilabel\">Administration Password</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+M</span></td><td><span class=\"guilabel\">MAC Address Spoofing</td></tr>\n"
"<tr><td><span class=\"keycap\">Ctrl+Shift+N</span></td><td><span class=\"guilabel\">Network Configuration</td></tr>\n"
"<tr><td><span class=\"keycap\">Alt+S</td><td><span class=\"guilabel\">Start Tails</td></tr>\n"
"</table>\n"
msgstr ""

#~ msgid ""
#~ "<a id=\"boot_loader_menu\"></a>\n"
#~ "<a id=\"boot_menu\"></a> <!-- for backward compatibility -->\n"
#~ msgstr ""
#~ "<a id=\"boot_loader_menu\"></a>\n"
#~ "<a id=\"boot_menu\"></a> <!-- for backward compatibility -->\n"

#~ msgid "<div class=\"tip\">\n"
#~ msgstr "<div class=\"tip\">\n"

#~ msgid "</div>\n"
#~ msgstr "</div>\n"
